/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelosComposite;
//esta es la etiqueta

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Contrato implements Componente{
    //string codigo, objetito tipo cotizante, arreglo objeto tipo beneficiario
    private String codigo;
    private Cotizante cot;
    private List<Beneficiario> beneficiario;
    public String dato1, dato2;

    public Contrato(String codigo, Cotizante cot) {
        this.codigo = codigo;
        this.cot = cot;
        beneficiario = new ArrayList<Beneficiario>();
    }

    public void agregar(Beneficiario cot) {
        beneficiario.add(cot);
    }

    public void eliminar(Beneficiario cot) {
        beneficiario.remove(cot);
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Cotizante getCot() {
        return cot;
    }

    public void setCot(Cotizante cot) {
        this.cot = cot;
    }

    public List<Beneficiario> getBeneficiario() {
        return beneficiario;
    }

    public void setBenef(List<Beneficiario> beneficiario) {
        this.beneficiario = beneficiario;
    }
    
    @Override
    public String mostrar() {
        
        dato1 = this.codigo + cot.getNombre() + cot.getTelefono() ;
        for (Beneficiario beneficiario : beneficiario){
            dato1 += beneficiario.toString();
        }
        return dato1;
 
    }

    @Override
    public String mostrar(String par) {
        
        if(this.codigo.equals(par)){
        dato2 = this.codigo + cot.getNombre() + cot.getTelefono() ;
        for (Beneficiario beneficiario : beneficiario){
           dato2 +=  beneficiario.toString();
        }
        }
        return dato2;
    }

    
  
    
}
