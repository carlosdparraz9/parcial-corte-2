/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelosComposite;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Arrays;


public class EPS implements Componente{
    
    String S1, S2;

    private ArrayList<Componente> componente;
    private String Codigo;

    public EPS(String Codigo) {
        this.Codigo = Codigo;
        componente = new ArrayList<Componente>();
    }
    public void add(Componente c) {
        this.componente.add(c);
    }

    public void remover(Componente c) {
        this.componente.remove(c);
    }

    @Override
    public String mostrar() {
        componente.forEach((c)->{
            S1 = c.mostrar();
            });
        return S1;
    }

    @Override
    public String mostrar(String codigo) {
        componente.forEach((c) -> {
            S2 = c.mostrar(codigo);
        });
        return S2;
    } 
}
