/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelosComposite;


public class Beneficiario {
    
  //nombre, documento, fecha y genero 
    private String nombre = "";
    private String documento = "";
    private String fechaN = "";
    private String genero = "";

    public Beneficiario (){
        
    }
    public Beneficiario(String nombre, String documento, String fechaN, String genero) {
        this.nombre = nombre;
        this.documento = documento;
        this.fechaN = fechaN;
        this.genero = genero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getFechaN() {
        return fechaN;
    }

    public void setFechaN(String fechaN) {
        this.fechaN = fechaN;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return nombre+ documento+ fechaN + genero;
    }
    
    
    
}
