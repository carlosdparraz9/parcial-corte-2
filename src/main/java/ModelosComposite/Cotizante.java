/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelosComposite;


public class Cotizante {
    
    private String nombre;
    private String direccion;
    private String documento;
    private String fechaN;
    private String genero;
    private String telefono;

    public Cotizante(String nombre, String direccion, String documento, String fechaN, String genero, String telefono) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.documento = documento;
        this.fechaN = fechaN;
        this.genero = genero;
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getFechaN() {
        return fechaN;
    }

    public void setFechaN(String fechaN) {
        this.fechaN = fechaN;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return nombre+ direccion+ documento+ fechaN+ genero+ telefono;
    }
    
    
    
}
