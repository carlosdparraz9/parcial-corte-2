
package ModelosAdapter;


public class AdapterAdministrador extends StakeHolder {

    private Administrador administrador;

    public AdapterAdministrador(String login, String password) {
         super();
         this.administrador = new Administrador(login, password);
        
    }

    public boolean ingresar(String login, String password) {
        return this.administrador.entrar(login, password);
    }

    @Override
    public String getTipodeStakeholder() {
        return this.administrador.retornarTipo();
    }

    @Override
    public String getUsuario() {
        return this.administrador.getUsuario();
    }

    @Override
    public void setUsuario(String usuario) {
        
        this.administrador.setUsuario(usuario);
        
    }

    @Override
    public String getPassword() {
        return this.administrador.getContrasena();
    }

    @Override
    public void setPassword(String contrasena) {
        this.administrador.setContrasena(contrasena);
    }

    @Override
    public boolean ingresar(String user, String password, Paciente paciente, Medico medico, Administrador administrador) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
