
package ModelosAdapter;


public class Administrador {

    private String usuario;
    private String contrasena;

    public boolean entrar(String usuario, String contrasena) {
        return (usuario.equals(this.usuario) && contrasena.equals(this.contrasena));
    }

    public Administrador(String usuario, String contrasena) {
        this.usuario = usuario;
        this.contrasena = contrasena;
    }

    public String retornarTipo() {
        return "Administrador";
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
}
