
package ModelosAdapter;


public class Medico extends StakeHolder {

    private String user;
    private String password;

    public Medico() {
    }

    public Medico(String user, String password) {
        this.user = user;
        this.password = password;
    }

  
    public boolean ingresar(String login, String password) {
        return (login.equals(this.user) && password.equals(this.password));
    }

    @Override
    public String getTipodeStakeholder() {
        return "Medico";
    }

    @Override
    public String getUsuario() {
        return user;
    }

    @Override
    public void setUsuario(String login) {
        this.user = login;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean ingresar(String user, String password, Paciente paciente, Medico medico, Administrador administrador) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
