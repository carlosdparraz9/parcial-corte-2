
package ModelosAdapter;


public abstract class StakeHolder {
    
    protected String Username;
    protected String Password;
    protected String TipoStakeHolder;
    Paciente paciente;
    Medico medico;
    Administrador administrador;

    public abstract boolean ingresar(String user, String password, Paciente paciente, Medico medico, Administrador administrador);

    public abstract String getTipodeStakeholder();

    public abstract String getUsuario();

    public abstract void setUsuario(String usuario);

    public abstract String getPassword();

    public abstract void setPassword(String contrasena);

    public StakeHolder() {
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getTipoStakeHolder() {
        return TipoStakeHolder;
    }

    public void setTipoStakeHolder(String TipoStakeHolder) {
        this.TipoStakeHolder = TipoStakeHolder;
    }
    
    

}
