
package Utilidades;

import ModelosFacade.Contenedor;
import ModelosFacade.Medico;
import ModelosFacade.Paciente;
import java.sql.Time;




public class Facade {

    private Contenedor contenedor;

    public Facade() {
        this.contenedor = new Contenedor();
    }

    public void agregarMedico(String nombre) {
        this.contenedor.addMedico(nombre);
    }

    public void agregarPaciente(String nombre) {
        this.contenedor.addPaciente(nombre);
    }

    public void registrarDisponibilidad(String nombre, String fecha, String horaManana, String horaTarde) {

        for (Medico medico : contenedor.getMedicos()) {
            if (medico.getNombre().equals(nombre)) {
                medico.setHoras(fecha, horaManana, horaTarde);
            }
        }

    }

    public String buscarDisponibilidadMedico(String nombre, String fecha) {
        return contenedor.getPacientes().get(0).buscarDisponibilidad(this.contenedor.getMedicos(), nombre, fecha).toString();
    }

    public void asignarCitasAMedicos(String nombre, String fecha, Time hora, String nombreMed) {
        for (Paciente paciente : contenedor.getPacientes()) {
            if (paciente.getNombre().equals(nombre)) {
                paciente.asignarCitas(contenedor.getMedicos(), fecha, hora, nombreMed);
            }
        }

    }

    public String consultarCitasMedico(String nombreMed) {
        for (Medico medico : contenedor.getMedicos()) {
            if (medico.getNombre().equals(nombreMed)) {
                return medico.mostrarCitas();
            }
        }
        return "no hay medico";
    }

    public String consultarCitasPaciente(String nombrePaci) {

        for (Paciente paciente : contenedor.getPacientes()) {
            if (paciente.getNombre().equals(nombrePaci)) {
                return paciente.mostrarCitas();
            }
        }
        return "no hay paciente";
    }
}
