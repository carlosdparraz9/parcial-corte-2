
package ModelosFacade;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;


public class Medico {

    private String nombre;
    private List<Cita> citas;

    public Medico() {
        this.citas = new ArrayList();
    }

    public Medico(String nombre, List<Cita> citas) {
        this.nombre = nombre;
        this.citas = citas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Cita> getCitas() {
        return citas;
    }

    public void setCitas(List<Cita> citas) {
        this.citas = citas;
    }

    @Override
    public String toString() {
        return "Medico{" + "nombre=" + nombre + '}';
    }

    public void setHoras(String fecha, String hora, String hora2) {
        String[] horas = hora.split("-");
        Time horaCast = new Time(Integer.parseInt(horas[0]), 0, 0);

        Cita c = new Cita(fecha, horaCast, null, null, true);
        citas.add(c);

        for (int i = 0; i < (Integer.parseInt(horas[1]) - Integer.parseInt(horas[0])) * 2; i++) {
            Time horaNew = new Time(citas.get(i).getHora().getHours(), citas.get(i).getHora().getMinutes(), 0);
            horaNew.setMinutes(citas.get(i).getHora().getMinutes() + 30);
            Cita ca = new Cita(fecha, horaNew, null, null, true);
            citas.add(ca);

        }

        horas = hora2.split("-");
        horaCast = new Time(Integer.parseInt(horas[0]), 0, 0);

        c = new Cita(fecha, horaCast, null, null, true);

        citas.add(c);
        Integer tamano = citas.size() - 1;

        for (int i = 0; i < (Integer.parseInt(horas[1]) - Integer.parseInt(horas[0])) * 2; i++) {
            Time horaNew = new Time(citas.get(tamano + i).getHora().getHours(), citas.get(tamano + i).getHora().getMinutes(), 0);
            horaNew.setMinutes(horaNew.getMinutes() + 30);
            Cita ca = new Cita(fecha, horaNew, null, null, true);
            citas.add(ca);

        }
    }

    public String mostrarCitas() {
        String text = "";
        for (Cita cita : this.citas) {

            if (!cita.isDisponible()) {
                text = this.nombre + " tiene citas :" + cita.toString();
            }
        }

        return text;

    }
}
