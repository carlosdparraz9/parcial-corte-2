
package ModelosFacade;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;


public class Paciente {

    private String nombre;
    private List<Cita> citas;

    public Paciente() {
        this.citas = new ArrayList();
    }

    public Paciente(String nombre, List<Cita> citas) {
        this.nombre = nombre;
        this.citas = citas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Cita> buscarDisponibilidad(ArrayList<Medico> medicos, String nombre, String fecha) {
        ArrayList<Cita> citasDispo = new ArrayList();
        for (Medico medico : medicos) {
            if (medico.getNombre().equals(nombre)) {

                for (Cita cita : medico.getCitas()) {

                    if (cita.getFecha().equals(fecha) && cita.isDisponible()) {

                        citasDispo.add(cita);
                    }

                }

            }

        }
        return citasDispo;
    }

    public void asignarCitas(ArrayList<Medico> medicos, String fecha, Time hora, String nombreMed) {
        for (Medico medico : medicos) {
            if (medico.getNombre().equals(nombreMed)) {
                for (Cita cita : medico.getCitas()) {
                    if (cita.getFecha().equals(fecha) && cita.getHora().equals(hora)) {
                        cita.setPaciente(this);
                        cita.setMedico(medico);
                        cita.setDisponible(false);
                        this.citas.add(cita);
                    }

                }
            }
        }

    }

    public String mostrarCitas() {
        String text;
        text = this.nombre + " tiene citas :" + this.citas.toString();
        return text;

    }

    @Override
    public String toString() {
        return "Paciente{" + "nombre=" + nombre + '}';
    }

    public List<Cita> getCitas() {
        return citas;
    }

    public void setCitas(List<Cita> citas) {
        this.citas = citas;
    }

}
