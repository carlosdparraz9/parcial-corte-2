
package ModelosFacade;

import java.sql.Time;


public class Cita {

    private String fecha;
    private Time hora;
    private Medico medico;
    private Paciente paciente;
    private boolean disponible;

    public Cita(String fecha, Time hora, Medico medico, Paciente paciente, boolean disponible) {
        this.fecha = fecha;
        this.hora = hora;
        this.medico = medico;
        this.paciente = paciente;
        this.disponible = disponible;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Cita() {
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    @Override
    public String toString() {
        return "Cita{" + "fecha=" + fecha + ", hora=" + hora + ", medico=" + medico + ", paciente=" + paciente + ", disponible=" + disponible + '}';
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

}
