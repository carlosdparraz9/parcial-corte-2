package Composite;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ModelosComposite.Beneficiario;
import ModelosComposite.Componente;
import ModelosComposite.Contrato;
import ModelosComposite.Cotizante;
import ModelosComposite.EPS;

public class Prueba {
    
    public Prueba() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void pruebaMostrar1() {
     
        Cotizante c = new Cotizante("P", "1", "2", "3", "M", "4");
        Beneficiario b1 = new Beneficiario("carlos", "calle 23 n 108", "1016103769", "M");
        Beneficiario b2 = new Beneficiario("Daniel", "carrera 108 n 23", "2209712441", "M");

        Componente con1 = new Contrato("1", c);
        
        Contrato contrato = (Contrato) con1;
        
        contrato.agregar(b1);
        contrato.agregar(b2);
        
         EPS ips = new EPS("Medimas");
        ips.add(con1);
        
        EPS eps = new EPS("Sanitas");
        eps.add(ips);
        
        assertEquals(eps.mostrar(),contrato.mostrar());

        
}
    
    @Test
    public void pruebaMostrar2() {
        Cotizante c = new Cotizante("Carlos", "Cra 108 n 23", "1016103769", "19-07-1998", "M", "3124280809");
        Beneficiario b1 = new Beneficiario("carlos", "calle 23 n 108", "1016103769", "M");
        Beneficiario b2 = new Beneficiario("Alejandra", "km 7 autopista bogota medellin", "2", "F");

        Componente con1 = new Contrato("1", c);
        
        Contrato contrato = (Contrato) con1;
        
        contrato.agregar(b1);
        contrato.agregar(b2);
        
        EPS ips = new EPS("Cafesalud");
        ips.add(con1);
        
        EPS eps = new EPS("Sanitas");
        eps.add(ips);
        
        
        
        assertEquals(ips.mostrar("1"), contrato.mostrar());
    }

    
}
