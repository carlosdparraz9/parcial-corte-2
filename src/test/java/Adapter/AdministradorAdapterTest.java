package Adapter;


import org.junit.Test;
import static org.junit.Assert.*;
import ModelosAdapter.Medico;
import ModelosAdapter.AdapterAdministrador;
import ModelosAdapter.Administrador;
import ModelosAdapter.Paciente;
import ModelosAdapter.StakeHolder;




public class AdministradorAdapterTest {
    
    public AdministradorAdapterTest() {
    }
@Test
    public void AdapterAdministradorIngresar() {
        StakeHolder C = new AdapterAdministrador("usuario","password");
        C.setUsuario("admin");
        C.setPassword("admin");
        
       // assertFalse (C.ingresar("Admin","Admin"));
    }
    @Test
    public void testAdapterAdministradorIngresarFalla() {
        StakeHolder C = new AdapterAdministrador("admin","admin");
        C.setUsuario("admin");
        C.setPassword("admin");
        
     //  assertFalse (C.ingresar("Admin","Admin"));
    }
    
@Test
    public void testGettAdapter() {
        StakeHolder C = new AdapterAdministrador("admin","admin");    
        String result = "Administrador"; 
        
        assertEquals(C.getTipodeStakeholder(), result);
    }
    
    @Test
    public void testGettFallaAdapter() {
        StakeHolder C = new AdapterAdministrador("admin","admin");    
        String result = "Otro"; 
        
        assertNotEquals(C.getTipodeStakeholder(), result);
    }
}